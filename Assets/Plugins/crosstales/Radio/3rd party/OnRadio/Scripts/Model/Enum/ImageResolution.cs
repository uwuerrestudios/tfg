﻿namespace Crosstales.Radio.OnRadio.Model
{
   /// <summary>All possible image resolutions.</summary>
   public enum ImageResolution
   {
      low,
      med,
      hi
   }
}
// © 2020 crosstales LLC (https://www.crosstales.com)
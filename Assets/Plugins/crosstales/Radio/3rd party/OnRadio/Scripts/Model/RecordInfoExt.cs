﻿namespace Crosstales.Radio.OnRadio.Model
{
	/// <summary>Extended RecordInfo.</summary>
	public class RecordInfoExt : Crosstales.Radio.Model.RecordInfo
	{
		//public string StationId;

		public RadioStationExt Station;

		public RecordInfoExt(string title, string artist, RadioStationExt station)
		{
			Title = title.Trim();
			Artist = artist.Trim();
			Station = station;
		}
	}
}
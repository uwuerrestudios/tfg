﻿using UnityEngine;

namespace Crosstales.Radio.Demo
{
   /// <summary>Simple test script for all UnityEvent-callbacks.</summary>
   [ExecuteInEditMode]
   [HelpURL("https://www.crosstales.com/media/data/assets/radio/api/class_crosstales_1_1_radio_1_1_demo_1_1_event_tester.html")]
   public class EventTester : MonoBehaviour
   {
      public Crosstales.Radio.Set.RadioSet Set;

      public void OnPlaybackStart(string name, int hash)
      {
         Debug.Log("OnPlaybackStart: " + name + " - " + hash);

         Debug.LogWarning(Set.StationFromHashCode(hash));
      }

      public void OnPlaybackEnd(string name, int hash)
      {
         Debug.Log("OnPlaybackEnd: " + name + " - " + hash);

         //Debug.LogWarning(Set.StationFromHashCode(hash));
      }

      public void OnRecordChange(string name, int hash)
      {
         Debug.Log("OnRecordChange: " + name + " - " + hash);

         Debug.LogWarning(Set.StationFromHashCode(hash));
      }

      public void OnStationChange(string name, int hash)
      {
         Debug.Log("OnStationChange: " + name + " - " + hash);

         Debug.LogWarning(Set.StationFromHashCode(hash));
      }

      public void OnFilterChange()
      {
         Debug.Log("OnFilterChange");
      }

      public void OnError(string name, int hash, string info)
      {
         Debug.LogWarning("OnError: " + name + " - " + hash + " - " + info);

         //Debug.LogWarning(Set.StationFromHashCode(hash));
      }

      public void OnStationsChange()
      {
         Debug.Log("OnStationsChange");
      }

      public void OnProviderReady()
      {
         Debug.Log("OnProviderReady");
      }

      public void OnQueryComplete(string id)
      {
         Debug.Log("OnQueryComplete: " + id);
      }
   }
}
// © 2020 crosstales LLC (https://www.crosstales.com)
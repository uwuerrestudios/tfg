﻿using UnityEngine;

namespace Crosstales.Radio
{
   /// <summary>Multi-threaded player for a radio station (experimental).</summary>
   [HelpURL("https://www.crosstales.com/media/data/assets/radio/api/class_crosstales_1_1_radio_1_1_radio_player_experimental.html")]
   public class RadioPlayerExperimental : RadioPlayer
   {
      private const int Samples = 88200;
#if (!UNITY_WSA && !UNITY_WEBGL) || UNITY_EDITOR
#if UNITY_ANDROID
      private const int SampleSleep = 750;
#else
      private const int SampleSleep = 900;
#endif
      protected System.IO.Stream sampleStream;

      protected System.Threading.Thread worker2;

      protected override void readPCMData(float[] data)
      {
         if (playback && !stopped && bufferAvailable && sampleStream != null && sampleStream.Length > data.Length * 2)
         {
            byte[] buffer = new byte[data.Length * 2];

            try
            {
               //lock (sampleStream)
               //{
               int count;
               if ((count = sampleStream.Read(buffer, 0, buffer.Length)) > 0)
               {
#if CT_BLOCKCOPY
                  System.Buffer.BlockCopy(Util.Helper.ConvertByteArrayToFloatArray(buffer, count), 0, data, 0, count * 2); //TODO new, test it!
                  //System.Buffer.BlockCopy(Util.Helper.ConvertByteArrayToFloatArray(buffer, count), 0, data, 0, data.Length * 4);
#else
                  System.Array.Copy(Util.Helper.ConvertByteArrayToFloatArray(buffer, count), 0, data, 0, data.Length);
#endif
               }
               else
               {
                  //#if !UNITY_ANDROID
                  logNoMoreData(); //TODO enable for Android
                  //#endif
                  //Debug.Log("readPCMData: No data!");
               }

               //}
            }
            catch (System.Exception ex)
            {
               logDataError(ex);
            }
         }
         else
         {
#if CT_BLOCKCOPY
            System.Buffer.BlockCopy(new float[data.Length], 0, data, 0, data.Length); //TODO new, test it!
            //System.Buffer.BlockCopy(new float[data.Length], 0, data, 0, data.Length * 4);
#else
            System.Array.Copy(new float[data.Length], 0, data, 0, data.Length);
#endif
         }

         if (CaptureDataStream && DataStream != null)
         {
            byte[] bytes = Util.Helper.ConvertFloatArrayToByteArray(data, data.Length);
            lock (DataStream)
            {
               DataStream.Write(bytes, 0, bytes.Length);
            }
         }
      }

      private void readSamples(out System.IO.Stream _ms, ref bool worker2Abort)
      {
         using (_ms = new Util.MemoryCacheStream(2 * Util.Constants.FACTOR_MB, 16 * Util.Constants.FACTOR_MB))
            //using (_ms = new Util.PipeStream())
         {
            //((Util.PipeStream)ms).MaxBufferLength = 2 * Util.Constants.FACTOR_MB;

            //int workingCounter = 0;

            try
            {
               while (!bufferAvailable && !worker2Abort)
               {
                  System.Threading.Thread.Sleep(20);
               }

               do
               {
                  int _readSamples;
                  if (Codec == Model.Enum.AudioCodec.MP3_NLayer)
                  {
                     if (nLayerReader != null)
                     {
                        float[] buffer = new float[Samples];
                        //byte[] buffer = new byte[Samples];

                        if ((_readSamples = nLayerReader.ReadSamples(buffer, 0, buffer.Length)) > 0)
                        {
                           //workingCounter++;
                           //Debug.Log(System.DateTime.Now + " readSamples: " + workingCounter);

                           //lock (_ms)
                           //{
                           _ms.Write(Util.Helper.ConvertFloatArrayToByteArray(buffer, _readSamples), 0, _readSamples * 2);
                           //_ms.Write(buffer, 0, _readSamples);
                           //}
                        }
                        else
                        {
                           Debug.Log(System.DateTime.Now + " readSamples: No data!", this);
                           //logNoMoreData();
                        }
                     }
                  }
#if (UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN) && !UNITY_EDITOR_OSX && !UNITY_EDITOR_LINUX
                  else if (Codec == Model.Enum.AudioCodec.MP3_NAudio)
                  {
                     if (nAudioReader != null)
                     {
                        //byte[] buffer = new byte[Samples * 2];
                        byte[] buffer = new byte[Samples * 2];

                        //lock (ms)
                        //{
                        if ((_readSamples = nAudioReader.Read(buffer, 0, buffer.Length)) > 0)
                        {
                           //workingCounter++;
                           //Debug.Log(System.DateTime.Now + " readSamples: " + workingCounter);

                           //lock (_ms)
                           //{
                           _ms.Write(buffer, 0, _readSamples);
                           //}
                        }

                        //}
                     }
                  }
#endif
                  else if (Codec == Model.Enum.AudioCodec.OGG_NVorbis)
                  {
                     if (nVorbisReader != null)
                     {
                        float[] buffer = new float[Samples];

                        //lock (ms)
                        //{
                        //lock (nVorbisReader)
                        //{
                        if ((_readSamples = nVorbisReader.ReadSamples(buffer, 0, buffer.Length)) > 0)
                        {
                           //workingCounter++;
                           //Debug.Log(System.DateTime.Now + " readSamples: " + workingCounter);

                           //lock (_ms)
                           //{
                           //_ms.Write(Util.Helper.ConvertFloatArrayToByteArray(buffer, buffer.Length), 0, _readSamples * 2);
                           _ms.Write(Util.Helper.ConvertFloatArrayToByteArray(buffer, _readSamples), 0, _readSamples * 2);
                           //}
                        }

                        //}
                        //}
                     }
                  }

                  System.Threading.Thread.Sleep(SampleSleep);
               } while (!worker2Abort);
            }
            catch (System.Exception ex)
            {
               logDataError(ex);
            }
         }

         //Debug.LogWarning("Thread bb!");

         //sampleStream = null;
      }

      protected override void readStream(ref Model.RadioStation _station, ref bool _playback, ref System.IO.Stream _ms, ref bool _error, ref string _errorMessage, ref Model.RecordInfo _nextRecordInfo, ref float _nextRecordDelay)
      {
         if (_station.Url.StartsWith(Util.Constants.PREFIX_HTTP) || _station.Url.StartsWith(Util.Constants.PREFIX_HTTPS))
         {
            try
            {
               System.Net.ServicePointManager.ServerCertificateValidationCallback = Util.Helper.RemoteCertificateValidationCallback;

               using (Common.Util.CTWebClient client = new Common.Util.CTWebClient(int.MaxValue))
               {
                  System.Net.HttpWebRequest _request = (System.Net.HttpWebRequest)client.CTGetWebRequest(_station.Url.Trim());

                  // clear old request header and build own header to receive ICY-metadata
                  _request.Headers.Clear();
                  _request.Headers.Add("GET", "/ HTTP/1.1");
                  _request.Headers.Add("Icy-MetaData", "1"); // needed to receive metadata information
                  _request.UserAgent = "WinampMPEG/5.09";
                  //request.KeepAlive = true;

                  using (System.Net.HttpWebResponse _response = (System.Net.HttpWebResponse)_request.GetResponse())
                  {
                     // read blocksize to find metadata header
                     int _metaint = int.MaxValue;

                     if (!string.IsNullOrEmpty(_response.GetResponseHeader("icy-metaint")))
                     {
                        int.TryParse(_response.GetResponseHeader("icy-metaint"), out _metaint);
                     }

                     if (Util.Constants.DEV_DEBUG)
                        Debug.LogWarning("icy-metaint: " + _metaint);

                     // server info
                     string serverInfo = string.IsNullOrEmpty(_response.GetResponseHeader("icy-notice2")) ? _station.ServerInfo : _response.GetResponseHeader("icy-notice2");
                     _station.ServerInfo = serverInfo;

                     if (Util.Constants.DEV_DEBUG)
                        Debug.LogWarning("icy-notice2: " + serverInfo);

                     if (_station.UpdateDataAtPlay)
                     {
                        // name
                        string _name = string.IsNullOrEmpty(_response.GetResponseHeader("icy-name")) ? _station.Name : _response.GetResponseHeader("icy-name");
                        _station.Name = _name;

                        if (Util.Constants.DEV_DEBUG)
                           Debug.LogWarning("icy-name: " + _name);

                        // url
                        if (string.IsNullOrEmpty(_station.Station))
                        {
                           string url = string.IsNullOrEmpty(_response.GetResponseHeader("icy-url")) ? _station.Station : _response.GetResponseHeader("icy-url");
                           _station.Station = url;

                           if (Util.Constants.DEV_DEBUG)
                              Debug.LogWarning("icy-url: " + url);
                        }

                        // genres
                        string genres = string.IsNullOrEmpty(_response.GetResponseHeader("icy-genre")) ? _station.Genres : _response.GetResponseHeader("icy-genre");
                        _station.Genres = genres;

                        if (Util.Constants.DEV_DEBUG)
                           Debug.LogWarning("icy-genre: " + genres);

                        // bitrate
                        if (!string.IsNullOrEmpty(_response.GetResponseHeader("icy-br")))
                        {
                           if (int.TryParse(_response.GetResponseHeader("icy-br"), out int bitrate))
                              _station.Bitrate = Util.Helper.NearestBitrate(bitrate, _station.Format);
                        }

                        if (Util.Constants.DEV_DEBUG)
                           Debug.LogWarning("icy-br: " + _station.Bitrate);
                     }

                     using (System.IO.Stream _stream = _response.GetResponseStream())
                     {
                        if (_stream != null)
                        {
                           byte[] _buffer = new byte[_station.ChunkSize * Util.Constants.FACTOR_KB];
                           _playback = true;
                           Util.Context.TotalDataRequests++;
                           _station.TotalDataRequests++;

                           int _status = 0;
                           bool _isFirsttime = true;

                           _nextRecordDelay = 0f;

                           bool worker2Abort = false;

                           worker2 = new System.Threading.Thread(() => readSamples(out sampleStream, ref worker2Abort));
                           worker2.Start();

                           do
                           {
                              int _read;
                              if ((_read = _stream.Read(_buffer, 0, _buffer.Length)) > 0)
                              {
                                 Util.Context.TotalDataSize += _read;
                                 _station.TotalDataSize += _read;

                                 int _offset = 0;

                                 if (_metaint > 0 && _read + _status > _metaint)
                                 {
                                    for (int ii = 0; ii < _read && _playback;)
                                    {
                                       if (_status == _metaint)
                                       {
                                          _status = 0;

                                          _ms.Write(_buffer, _offset, ii - _offset);
                                          _offset = ii;

                                          int _metadataLength = System.Convert.ToInt32(_buffer[ii]) * 16; // length of metadata header
                                          ii++;
                                          _offset++;

                                          if (_metadataLength > 0)
                                          {
                                             if (_metadataLength + _offset <= _read)
                                             {
                                                byte[] metaDataBuffer = new byte[_metadataLength];

                                                System.Array.Copy(_buffer, ii, metaDataBuffer, 0, _metadataLength);

                                                _nextRecordInfo = new Model.RecordInfo(System.Text.Encoding.UTF8.GetString(metaDataBuffer));

                                                if (!_isFirsttime)
                                                {
                                                   _nextRecordDelay = (float)(_ms.Length - _ms.Position) / (_station.Bitrate * 125);
                                                }
                                                else
                                                {
                                                   _isFirsttime = false;
                                                }

                                                ii += _metadataLength;
                                                _offset += _metadataLength;

                                                if (Util.Constants.DEV_DEBUG)
                                                   Debug.LogWarning("RecordInfo read: " + _nextRecordInfo);
                                             }
                                             else
                                             {
                                                if (Util.Constants.DEV_DEBUG)
                                                   Debug.LogError("Info-frame outside of the buffer!");

                                                ii = _read;
                                                _status = _read - (_metadataLength + _offset);
                                             }
                                          }
                                       }
                                       else
                                       {
                                          _status++;
                                          ii++;
                                       }
                                    }

                                    if (_offset < _read)
                                    {
                                       _ms.Write(_buffer, _offset, _read - _offset);
                                    }
                                 }
                                 else
                                 {
                                    _status += _read;
                                    _ms.Write(_buffer, 0, _read);
                                 }
                              }
                           } while (_playback);

                           worker2Abort = true;
                        }
                     }
                  }
               }
            }
            catch (System.Exception ex)
            {
               _error = true;
               _errorMessage = _station.Name + System.Environment.NewLine + "Could not read url after " + Util.Helper.FormatSecondsToHourMinSec(PlayTime) + "!" + System.Environment.NewLine + ex;
               Debug.LogError(_errorMessage);

               _playback = false;
            }
         }
         else
         {
            readStreamLegacy(ref _station, ref _playback, ref _ms, ref _error, ref _errorMessage);
         }
      }

      protected override void readStreamLegacy(ref Model.RadioStation _station, ref bool _playback, ref System.IO.Stream _ms, ref bool _error, ref string _errorMessage)
      {
         if (_station.Url.StartsWith("file://"))
         {
            Debug.LogWarning(Util.Constants.ASSET_NAME + " is intended for streams and not for files! Please consider using 'DJ' instead: https://goo.gl/54wMJv");
         }

         try
         {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = Util.Helper.RemoteCertificateValidationCallback;

            using (Common.Util.CTWebClient client = new Common.Util.CTWebClient(int.MaxValue))
            {
               using (System.Net.WebResponse _response = client.CTGetWebRequest(_station.Url.Trim()).GetResponse())
               {
                  using (System.IO.Stream _stream = _response.GetResponseStream())
                  {
                     if (_stream != null)
                     {
                        byte[] _buffer = new byte[_station.ChunkSize * Util.Constants.FACTOR_KB];
                        _playback = true;
                        Util.Context.TotalDataRequests++;
                        _station.TotalDataRequests++;

                        bool worker2Abort = false;

                        worker2 = new System.Threading.Thread(() => readSamples(out sampleStream, ref worker2Abort));
                        worker2.Start();

                        do
                        {
                           int _read;
                           if ((_read = _stream.Read(_buffer, 0, _buffer.Length)) > 0)
                           {
                              Util.Context.TotalDataSize += _read;
                              _station.TotalDataSize += _read;

                              if (_playback)
                              {
                                 //lock (_ms)
                                 //{
                                 _ms.Write(_buffer, 0, _read);
                                 //}
                              }
                           }
                        } while (_playback);

                        worker2Abort = true;
                     }
                  }
               }
            }
         }
         catch (System.Exception ex)
         {
            _error = true;
            _errorMessage = _station.Name + System.Environment.NewLine + "Could not read url after " + Util.Helper.FormatSecondsToHourMinSec(PlayTime) + "!" + System.Environment.NewLine + ex;
            Debug.LogError(_errorMessage);

            _playback = false;
         }
      }
      #endif
   }
}
// © 2019-2020 crosstales LLC (https://www.crosstales.com)
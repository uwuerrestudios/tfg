﻿using UnityEngine;

namespace Crosstales.Radio.Provider
{
   /// <summary>Provider for users of Radio. This enables the possibility to manage the desired stations with a given initial set of stations.</summary>
   [HelpURL("https://www.crosstales.com/media/data/assets/radio/api/class_crosstales_1_1_radio_1_1_provider_1_1_radio_provider_user.html")]
   public class RadioProviderUser : BaseRadioProvider
   {
      #region Variables

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("SaveOnDisable")] [Header("Save Behaviour"), Tooltip("Call 'Save' OnDisable (default: true)."), SerializeField]
      private bool saveOnDisable = true;


      [UnityEngine.Serialization.FormerlySerializedAsAttribute("Entry")] [Header("Source Settings"), Tooltip("User radio station entry."), SerializeField]
      private Model.Entry.RadioEntryUser entry;

      #endregion


      #region Properties

      /// <summary>Call 'Save' OnDisable.</summary>
      public bool SaveOnDisable
      {
         get => saveOnDisable;
         set => saveOnDisable = value;
      }

      /// <summary>User radio station entry.</summary>
      public Model.Entry.RadioEntryUser Entry
      {
         get => entry;
         private set => entry = value;
      }

      public override System.Collections.Generic.List<Model.Entry.BaseRadioEntry> RadioEntries => new System.Collections.Generic.List<Model.Entry.BaseRadioEntry> {entry};

      protected override StationsChangeEvent onStationsChanged => OnStationsChanged;

      protected override ProviderReadyEvent onProviderReadyEvent => OnProviderReadyEvent;

      #endregion


      #region Events

      [Header("Events")] public StationsChangeEvent OnStationsChanged;
      public ProviderReadyEvent OnProviderReadyEvent;

      #endregion


      #region MonoBehaviour methods

      public void OnDisable()
      {
         if (!Util.Helper.isEditorMode)
         {
            if (saveOnDisable)
            {
               Save(entry.FinalPath);
            }
         }
      }

      public override void OnValidate()
      {
         if (entry?.isInitialized == false)
            entry.LoadOnlyOnce = true;

         base.OnValidate();
      }

      #endregion


      #region Public methods

      /// <summary>Deletes the user text-file.</summary>
      public void Delete()
      {
         if (System.IO.File.Exists(entry.FinalPath))
         {
            try
            {
               System.IO.File.Delete(entry.FinalPath);
            }
            catch (System.IO.IOException ex)
            {
               Debug.LogError("Could not delete file: " + entry.FinalPath + System.Environment.NewLine + ex, this);
            }
         }
      }

      /// <summary>Shows the location of the user text-file in OS file browser.</summary>
      public void ShowFile()
      {
         Util.Helper.ShowFileLocation(entry.FinalPath);
      }

      /// <summary>Edits the user text-file with the OS default application.</summary>
      public void EditFile()
      {
         Util.Helper.OpenFile(entry.FinalPath);
      }

      #endregion


      #region Private methods

      protected override void init()
      {
         base.init();

         if (entry?.EnableSource == true)
         {
            if (!entry.AllowOnlyHTTPS && Util.Helper.isIOSPlatform) // always enable for iOS
               entry.AllowOnlyHTTPS = true;

            if (!string.IsNullOrEmpty(entry.FinalPath) && System.IO.File.Exists(entry.FinalPath))
               StartCoroutine(loadWeb(addCoRoutine(), new Model.Entry.RadioEntryURL(entry, Util.Helper.ValidURLFromFilePath(entry.FinalPath), Model.Enum.DataFormatURL.Text), true));

            if (entry.Resource != null)
            {
               if (!entry.LoadOnlyOnce || entry.LoadOnlyOnce && !System.IO.File.Exists(entry.FinalPath))
               {
                  StartCoroutine(loadResource(addCoRoutine(), new Model.Entry.RadioEntryResource(entry, entry.Resource, entry.DataFormat, entry.ReadNumberOfStations), true));

                  if (!System.IO.File.Exists(entry.FinalPath))
                  {
                     //always store file first
                     Invoke(nameof(save), 2f);
                  }
               }
            }
         }
      }

      private void save()
      {
         Save(entry.FinalPath);
      }

      #endregion


      #region Editor-only methods

#if UNITY_EDITOR

      protected override void initInEditor()
      {
         if (Util.Helper.isEditorMode)
         {
            base.initInEditor();

            if (entry?.EnableSource == true)
            {
               if (!entry.AllowOnlyHTTPS && Util.Helper.isIOSPlatform) // always enable for iOS
                  entry.AllowOnlyHTTPS = true;

               if (!string.IsNullOrEmpty(entry.FinalPath) && System.IO.File.Exists(entry.FinalPath))
                  loadWebInEditor(new Model.Entry.RadioEntryURL(entry, Util.Helper.ValidURLFromFilePath(entry.FinalPath), Model.Enum.DataFormatURL.Text), true);

               if (entry.Resource != null)
               {
                  if (!entry.LoadOnlyOnce || entry.LoadOnlyOnce && !System.IO.File.Exists(entry.FinalPath))
                     loadResourceInEditor(new Model.Entry.RadioEntryResource(entry, entry.Resource, entry.DataFormat, entry.ReadNumberOfStations), true);
               }
            }
         }
      }

#endif

      #endregion
   }
}
// © 2016-2020 crosstales LLC (https://www.crosstales.com)
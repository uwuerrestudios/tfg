﻿namespace Crosstales.Radio.Model.Enum
{
   /// <summary>Prefixes for URLs.</summary>
   public enum URLPrefix
   {
      None,
      Http,
      Https,
      File,
      PersistentDataPath,
      DataPath,
      TempPath
   }
}
// © 2016-2020 crosstales LLC (https://www.crosstales.com)
﻿using UnityEngine;

namespace Crosstales.Radio.Model
{
   /// <summary>Filter for radio stations.</summary>
   [System.Serializable]
   public class RadioFilter
   {
      #region Variables

      /// <summary>Part of the radio names (callsigns).</summary>
      [Tooltip("Part of the radio names (callsigns).")] public string Names = string.Empty;

      /// <summary>Part of the radio URLs.</summary>
      [Tooltip("Part of the radio URLs.")] public string Urls = string.Empty;

      /// <summary>Part of the radio stations.</summary>
      [Tooltip("Part of the radio stations.")] public string Stations = string.Empty;

      /// <summary>Part of the radio genres.</summary>
      [Tooltip("Part of the radio genres.")] public string Genres = string.Empty;

      /// <summary>Part of the radio cities.</summary>
      [Tooltip("Part of the radio cities.")] public string Cities = string.Empty;

      /// <summary>Part of the radio countries (ISO 3166-1, e.g. 'ch').</summary>
      [Tooltip("Part of the radio countries (ISO 3166-1, e.g. 'ch').")] public string Countries = string.Empty;

      /// <summary>Part of the radio languages (like 'german').</summary>
      [Tooltip("Part of the radio languages (like 'german').")] public string Languages = string.Empty;

      /// <summary>Minimal rating (default: 0).</summary>
      [Range(0f, 4.9f)] [Tooltip("Minimal rating (default: 0).")] public float RatingMin = 0f;

      /// <summary>Maximal rating (default: 5).</summary>
      [Range(0.1f, 5f)] [Tooltip("Maximal rating (default: 5).")] public float RatingMax = 5f;

      /// <summary>Part of the radio formats.</summary>
      [Tooltip("Part of the radio formats.")] public string Format = string.Empty;

      /// <summary>Minimal bitrate in kbit/s (default: 32).</summary>
      [Range(32, 499)] [Tooltip("Minimal bitrate in kbit/s (default: 32).")] public int BitrateMin = 32;

      /// <summary>Maximal bitrate in kbit/s (default: 500).</summary>
      [Range(33, 500)] [Tooltip("Maximal bitrate in kbit/s (default: 500).")] public int BitrateMax = 500;

      /// <summary>Exclude radio stations with unsupported codecs (default: true).</summary>
      [Tooltip("Exclude radio stations with unsupported codecs (default: true).")] public bool ExcludeUnsupportedCodecs = true;

      /// <summary>Limit number of results (default: 0 = unlimited).</summary>
      [Range(0, 500)] [Tooltip("Limit number of results (default: 0 = unlimited).")] public int Limit = 0;

      #endregion


      #region Properties

      /// <summary>Are filter parameters set and active?</summary>
      /// <returns>True if filter parameters are set and active.</returns>
      public bool isFiltering =>
         !string.IsNullOrEmpty(Names) ||
         !string.IsNullOrEmpty(Urls) ||
         !string.IsNullOrEmpty(Stations) ||
         !string.IsNullOrEmpty(Genres) ||
         !string.IsNullOrEmpty(Cities) ||
         !string.IsNullOrEmpty(Countries) ||
         !string.IsNullOrEmpty(Languages) ||
         RatingMin > 0f ||
         RatingMax < 5f ||
         !string.IsNullOrEmpty(Format) ||
         BitrateMin > 32 ||
         BitrateMax < 500 ||
         Limit != 0 ||
         !ExcludeUnsupportedCodecs;

      #endregion


      #region Constructors

      /// <summary>Default-constructor for a RadioFilter.</summary>
      public RadioFilter()
      {
         //empty
      }

      /// <summary>Clone-constructor for a RadioFilter.</summary>
      public RadioFilter(RadioFilter filter)
      {
         Names = filter.Names;
         Urls = filter.Urls;
         Stations = filter.Stations;
         Genres = filter.Genres;
         Cities = filter.Cities;
         Countries = filter.Countries;
         Languages = filter.Languages;
         RatingMin = filter.RatingMin;
         RatingMax = filter.RatingMax;
         Format = filter.Format;
         BitrateMin = filter.BitrateMin;
         BitrateMax = filter.BitrateMax;
         ExcludeUnsupportedCodecs = filter.ExcludeUnsupportedCodecs;
         Limit = filter.Limit;
      }

      #endregion


      #region Overridden methods

      public override bool Equals(object obj)
      {
         // Check for null values and compare run-time types.
         if (obj == null || GetType() != obj.GetType())
            return false;

         RadioFilter rf = (RadioFilter)obj;

         return Names == rf.Names &&
                Urls == rf.Urls &&
                Stations == rf.Stations &&
                Genres == rf.Genres &&
                Cities == rf.Cities &&
                Countries == rf.Countries &&
                Languages == rf.Languages &&
                System.Math.Abs(RatingMin - rf.RatingMin) < Util.Constants.FLOAT_TOLERANCE &&
                System.Math.Abs(RatingMax - rf.RatingMax) < Util.Constants.FLOAT_TOLERANCE &&
                Format == rf.Format &&
                BitrateMin == rf.BitrateMin &&
                BitrateMax == rf.BitrateMax &&
                Limit == rf.Limit;
      }

      public override int GetHashCode()
      {
         int hash = 0;

         if (Names != null)
            hash += Names.GetHashCode();
         if (Urls != null)
            hash += Urls.GetHashCode();
         if (Stations != null)
            hash += Stations.GetHashCode();
         if (Genres != null)
            hash += Genres.GetHashCode();
         if (Cities != null)
            hash += Cities.GetHashCode();
         if (Countries != null)
            hash += Countries.GetHashCode();
         if (Languages != null)
            hash += Languages.GetHashCode();
         hash += (int)(RatingMin * 10);
         hash += (int)(RatingMax * 10);
         if (Format != null)
            hash += Format.GetHashCode();
         hash += BitrateMin;
         hash += BitrateMax;
         hash += Limit;

         return hash;
      }

      public override string ToString()
      {
         System.Text.StringBuilder result = new System.Text.StringBuilder();

         result.Append(GetType().Name);
         result.Append(Util.Constants.TEXT_TOSTRING_START);

         result.Append("Name='");
         result.Append(Names);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Url='");
         result.Append(Urls);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Station='");
         result.Append(Stations);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Genres='");
         result.Append(Genres);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Cities='");
         result.Append(Cities);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Countries='");
         result.Append(Countries);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Languages='");
         result.Append(Languages);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("RatingMin='");
         result.Append(RatingMin);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("RatingMax='");
         result.Append(RatingMax);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Format='");
         result.Append(Format);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("BitrateMin='");
         result.Append(BitrateMin);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("BitrateMax='");
         result.Append(BitrateMax);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("ExcludeUnsupportedCodecs='");
         result.Append(ExcludeUnsupportedCodecs);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("Limit='");
         result.Append(Limit);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER);

         result.Append("isFiltering='");
         result.Append(isFiltering);
         result.Append(Util.Constants.TEXT_TOSTRING_DELIMITER_END);

         result.Append(Util.Constants.TEXT_TOSTRING_END);

         return result.ToString();
      }

      #endregion
   }
}
// © 2016-2020 crosstales LLC (https://www.crosstales.com)
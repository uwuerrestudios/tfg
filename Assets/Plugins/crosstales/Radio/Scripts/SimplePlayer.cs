﻿using UnityEngine;

namespace Crosstales.Radio
{
   /// <summary>Simple player.</summary>
   [ExecuteInEditMode]
   [HelpURL("https://www.crosstales.com/media/data/assets/radio/api/class_crosstales_1_1_radio_1_1_simple_player.html")]
   public class SimplePlayer : BasePlayer, Set.ISet
   {
      #region Variables

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("Player")] [Header("General Settings"), Tooltip("'RadioPlayer' from the scene."), SerializeField]
      private RadioPlayer player;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("Set")] [Tooltip("'RadioSet' from the scene."), SerializeField]
      private Set.RadioSet set;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("Filter")] [Tooltip("Global RadioFilter (active if no explicit filter is given)."), SerializeField]
      private Model.RadioFilter filter;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("RetryOnError")] [Header("Retry Settings"), Tooltip("Retry to start the radio on an error (default: false)."), SerializeField]
      private bool retryOnError = false;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("Retries")] [Tooltip("Defines how many times should the radio station restart after an error before giving up (default: 3)."), SerializeField]
      private int retries = 3;


      [UnityEngine.Serialization.FormerlySerializedAsAttribute("PlayOnStart")] [Header("Behaviour Settings"), Tooltip("Play a radio on start (default: false)."), SerializeField]
      private bool playOnStart = false;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("PlayEndless")] [Tooltip("Enable endless play (default: true)."), SerializeField]
      private bool playEndless = true;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("PlayRandom")] [Tooltip("Play the radio stations in random order (default: false)."), SerializeField]
      private bool playRandom = false;

      private bool playedOnStart = false;
      private bool stopped = true;
      private int invokeDelayCounter = 1;
      private bool started = false;
      private float lastPlaytime = float.MinValue;

      private bool isDirectionNext = true;

      private Model.RadioFilter currentFilter;

      private bool isProviderReady = false;

      #endregion


      #region Properties

      /// <summary>'RadioPlayer' from the scene.</summary>
      public RadioPlayer Player
      {
         get => player;
         set => player = value;
      }

      /// <summary>'RadioSet' from the scene.</summary>
      public Set.RadioSet Set
      {
         get => set;
         set => set = value;
      }

      /// <summary>Global RadioFilter (active if no explicit filter is given).</summary>
      public Model.RadioFilter Filter
      {
         get => filter;
         set => filter = value;
      }

      /// <summary>Retry to start the radio on an error.</summary>
      public bool RetryOnError
      {
         get => retryOnError;
         set => retryOnError = value;
      }

      /// <summary>Defines how many times should the radio station restart after an error before giving up.</summary>
      public int Retries
      {
         get => retries;
         set => retries = value;
      }

      /// <summary>Play a radio on start.</summary>
      public bool PlayOnStart
      {
         get => playOnStart;
         set => playOnStart = value;
      }

      /// <summary>Enable endless play.</summary>
      public bool PlayEndless
      {
         get => playEndless;
         set => playEndless = value;
      }

      /// <summary>Play the radio stations in random order.</summary>
      public bool PlayRandom
      {
         get => playRandom;
         set => playRandom = value;
      }

      public override Model.RadioStation Station
      {
         get => player != null ? player.Station : null;

         set
         {
            if (player != null)
               player.Station = value;
         }
      }

      public override bool HandleFocus
      {
         get => player != null && player.HandleFocus;

         set
         {
            if (player != null)
               player.HandleFocus = value;
         }
      }

      public override int CacheStreamSize
      {
         get => player != null ? player.CacheStreamSize : Util.Config.DEFAULT_CACHESTREAMSIZE * Util.Constants.FACTOR_KB;

         set
         {
            if (player != null)
               player.CacheStreamSize = value;
         }
      }

      public override bool LegacyMode
      {
         get => player != null && player.LegacyMode;

         set
         {
            if (player != null)
               player.LegacyMode = value;
         }
      }

      public override bool CaptureDataStream
      {
         get => player != null && player.CaptureDataStream;

         set
         {
            if (player != null)
               player.CaptureDataStream = value;
         }
      }

      public override AudioSource Source
      {
         get => player != null ? player.Source : null;

         protected set
         {
            //ignore
         }
      }

      public override Model.Enum.AudioCodec Codec
      {
         get => player != null ? player.Codec : Model.Enum.AudioCodec.MP3_NLayer;

         protected set
         {
            //ignore
         }
      }

      public override float PlayTime
      {
         get => player != null ? player.PlayTime : 0f;

         protected set
         {
            //ignore
         }
      }

      public override float BufferProgress
      {
         get => player != null ? player.BufferProgress : 0f;

         protected set
         {
            //ignore
         }
      }

      public override bool isPlayback => player != null && player.isPlayback;

      public override bool isAudioPlaying => player != null && player.isAudioPlaying;

      public override bool isBuffering => player != null && player.isBuffering;

      public override float RecordPlayTime
      {
         get => player != null ? player.RecordPlayTime : 0f;

         protected set
         {
            //ignore
         }
      }

      public override Model.RecordInfo RecordInfo => player != null ? player.RecordInfo : new Model.RecordInfo();

      public override Model.RecordInfo NextRecordInfo => player != null ? player.RecordInfo : new Model.RecordInfo();

      public override float NextRecordDelay => player != null ? player.NextRecordDelay : 0;

      public override long CurrentBufferSize => player != null ? player.CurrentBufferSize : Util.Config.DEFAULT_BUFFERSIZE;

      public override long CurrentDownloadSpeed => player != null ? player.CurrentDownloadSpeed : 0;

      public override Util.MemoryCacheStream DataStream
      {
         get => player != null ? player.DataStream : null;

         protected set
         {
            //ignore
         }
      }

      public override int Channels => player != null ? player.Channels : 2;

      public override int SampleRate => player != null ? player.SampleRate : 44100;

      public override float Volume
      {
         get => player != null ? player.Volume : 1f;

         set
         {
            if (player != null)
               player.Volume = value;
         }
      }

      public override float Pitch
      {
         get => player != null ? player.Pitch : 1f;

         set
         {
            if (player != null)
               player.Pitch = value;
         }
      }

      public override float StereoPan
      {
         get => player != null ? player.StereoPan : 1f;

         set
         {
            if (player != null)
               player.StereoPan = value;
         }
      }

      public override bool isMuted
      {
         get => player != null && player.isMuted;

         set
         {
            if (player != null)
               player.isMuted = value;
         }
      }

      protected override PlaybackStartEvent onPlaybackStarted => OnPlaybackStarted;

      protected override PlaybackEndEvent onPlaybackEnded => OnPlaybackEnded;

      protected override RecordChangeEvent onRecordChanged => OnRecordChanged;

      protected override ErrorEvent onError => OnError;

      #endregion


      #region Events

      [Header("Events")] public PlaybackStartEvent OnPlaybackStarted;
      public PlaybackEndEvent OnPlaybackEnded;
      public RecordChangeEvent OnRecordChanged;
      public StationChangeEvent OnStationChanged;
      public FilterChangeEvent OnFilterChanged;
      public StationsChangeEvent OnStationsChanged;
      public ProviderReadyEvent OnProviderReadyEvent;
      public ErrorEvent OnError;

      /// <summary>An event triggered whenever the filter changes.</summary>
      public event FilterChange OnFilterChange;

      /// <summary>An event triggered whenever the stations change.</summary>
      public event StationsChange OnStationsChange;

      /// <summary>An event triggered whenever all providers are ready.</summary>
      public event ProviderReady OnProviderReady;

      /// <summary>An event triggered whenever an radio station changes.</summary>
      public event StationChange OnStationChange;

      #endregion


      #region MonoBehaviour methods

      public void Start()
      {
         if (filter != null)
            currentFilter = new Model.RadioFilter(filter);
      }

      public void Update()
      {
         if (filter == null && currentFilter != null)
         {
            currentFilter = null;
            onFilterChange();
         }
         else if (filter != null && currentFilter == null ||
                  currentFilter?.Equals(filter) == false)
         {
            currentFilter = new Model.RadioFilter(filter);
            onFilterChange();
         }
      }

      public void OnEnable()
      {
         if (player != null && set != null)
         {
            // Subscribe event listeners
            player.OnPlaybackStart += onPlaybackStart;
            player.OnPlaybackEnd += onPlaybackEnd;
            player.OnAudioStart += onAudioStart;
            player.OnAudioEnd += onAudioEnd;
            player.OnAudioPlayTimeUpdate += onAudioPlayTimeUpdate;
            player.OnBufferingStart += onBufferingStart;
            player.OnBufferingEnd += onBufferingEnd;
            player.OnBufferingProgressUpdate += onBufferingProgressUpdate;
            player.OnErrorInfo += onErrorInfo;
            player.OnRecordChange += onRecordChange;
            player.OnRecordPlayTimeUpdate += onRecordPlayTimeUpdate;
            player.OnNextRecordChange += onNextRecordChange;
            player.OnNextRecordDelayUpdate += onNextRecordDelayUpdate;
            set.OnProviderReady += onProviderReady;
            set.OnStationsChange += onStationsChange;
            set.OnFilterChange += onFilterChange;
         }
         else
         {
            if (!Util.Helper.isEditorMode)
            {
               Debug.LogError("'Player' or 'Set' are null!", this);
            }
         }
      }

      public void OnDisable()
      {
         if (player != null && set != null)
         {
            // Unsubscribe event listeners
            player.OnPlaybackStart -= onPlaybackStart;
            player.OnPlaybackEnd -= onPlaybackEnd;
            player.OnAudioStart += onAudioStart;
            player.OnAudioEnd += onAudioEnd;
            player.OnAudioPlayTimeUpdate -= onAudioPlayTimeUpdate;
            player.OnBufferingStart -= onBufferingStart;
            player.OnBufferingEnd -= onBufferingEnd;
            player.OnBufferingProgressUpdate -= onBufferingProgressUpdate;
            player.OnErrorInfo -= onErrorInfo;
            player.OnRecordChange -= onRecordChange;
            player.OnRecordPlayTimeUpdate -= onRecordPlayTimeUpdate;
            player.OnNextRecordChange -= onNextRecordChange;
            player.OnNextRecordDelayUpdate -= onNextRecordDelayUpdate;
            set.OnProviderReady -= onProviderReady;
            set.OnStationsChange -= onStationsChange;
            set.OnFilterChange -= onFilterChange;
         }
      }

      public void OnValidate()
      {
         if (retries < 0)
            retries = 0;
      }

      #endregion


      #region Public methods

      /// <summary>Plays a radio (main use is for UI).</summary>
      public override void Play()
      {
         if (player != null)
         {
            float currentTime = Time.realtimeSinceStartup;

            if (lastPlaytime + Util.Constants.PLAY_CALL_SPEED < currentTime)
            {
               lastPlaytime = currentTime;

               Stop();

               if (string.IsNullOrEmpty(player.Station?.Url))
                  player.Station = set.NextStation(playRandom, getFilter());

               stopped = false;

               if (Util.Helper.isEditorMode)
               {
#if UNITY_EDITOR
                  player.PlayInEditor();
#endif
               }
               else
               {
                  Invoke(nameof(play), Util.Constants.INVOKE_DELAY);
               }
            }
            else
            {
               Debug.LogWarning("'Play' called too fast - please slow down!", this);
            }
         }
      }

      /// <summary>Plays the next radio (main use for UI).</summary>
      public void Next()
      {
         Next(playRandom);
      }

      /// <summary>Plays the next (normal/random) radio.</summary>
      /// <param name="random">Play a random radio station</param>
      /// <param name="filter">Filter (default: null, optional)</param>
      public void Next(bool random, Model.RadioFilter filter = null)
      {
         isDirectionNext = true;

         if (player != null && set != null)
         {
            player.Station = set.NextStation(random, getFilter(filter));

            Play();
         }
      }

      /// <summary>Plays the previous radio (main use for UI).</summary>
      public void Previous()
      {
         Previous(playRandom);
      }

      /// <summary>Plays the previous radio.</summary>
      /// <param name="random">Play a random radio station</param>
      /// <param name="filter">Filter (default: null, optional)</param>
      public void Previous(bool random, Model.RadioFilter filter = null)
      {
         isDirectionNext = false;

         if (player != null && set != null)
         {
            player.Station = set.PreviousStation(random, getFilter(filter));

            Play();
         }
      }

      /// <summary>Stops the radio station playback.</summary>
      public override void Stop()
      {
         if (player != null)
         {
            player.Stop();
         }

         stopped = true;
      }

      /// <summary>Silences the AudioSource on the RadioPlayer-component.</summary>
      public override void Silence()
      {
         if (player != null)
            player.Silence();
      }

      /// <summary>Restarts the playback of the radio-station.</summary>
      /// <param name="invokeDelay">Delay for the restart (default: 0.4, optional)</param>
      public override void Restart(float invokeDelay = Util.Constants.INVOKE_DELAY)
      {
         if (player != null)
            player.Restart(invokeDelay);
      }

      public override void Mute()
      {
         if (player != null)
            player.Mute();
      }

      public override void UnMute()
      {
         if (player != null)
            player.UnMute();
      }

      #endregion


      #region Private methods

      private void play()
      {
         player.Play();

         onStationChange(player.Station);
      }

      private void playInvoker()
      {
         if (started)
         {
            Play();
         }
      }

      private Model.RadioFilter getFilter(Model.RadioFilter filter = null)
      {
         if (filter?.isFiltering == true)
            return filter;

         return this.filter.isFiltering ? this.filter : null;
      }

      private void playEndlessly()
      {
         if (!stopped && playEndless)
         {
            invokeDelayCounter = 1;

            if (isDirectionNext)
            {
               Next(playRandom);
            }
            else
            {
               Previous(playRandom);
            }
         }
      }

      #endregion


      #region Callback & event-trigger methods

      protected override void onAudioStart(Model.RadioStation station)
      {
         started = true;

         base.onAudioStart(station);
      }

      protected override void onAudioEnd(Model.RadioStation station)
      {
         started = false;

         base.onAudioEnd(station);
      }

      protected override void onAudioPlayTimeUpdate(Model.RadioStation station, float _playtime)
      {
         if (_playtime > 30f) //reset restartCounter after 30 seconds
            invokeDelayCounter = 1;

         base.onAudioPlayTimeUpdate(station, _playtime);
      }

      protected override void onErrorInfo(Model.RadioStation station, string info)
      {
         //if (Util.Helper.isInternetAvailable)
         //{
         if (retryOnError && started)
         {
            if (invokeDelayCounter < retries)
            {
               Stop();

               Debug.LogWarning("Error occurred -> Restarting station." + System.Environment.NewLine + info, this);

               Invoke(nameof(playInvoker), Util.Constants.INVOKE_DELAY * invokeDelayCounter);

               invokeDelayCounter++;
            }
            else
            {
               if (playEndless)
               {
                  playEndlessly();
               }
               else
               {
                  Stop();

                  Debug.LogError("Restarting station failed more than " + retries + " times - giving up!" + System.Environment.NewLine + info, this);
               }
            }
         }
         else
         {
            if (playEndless)
            {
               playEndlessly();
            }
            else
            {
               Stop();

               Debug.LogError("Could not start the station '" + station.Name + "'! Please try another station. " + System.Environment.NewLine + info, this);
            }
         }
         //}

         base.onErrorInfo(station, info);
      }

      private void onProviderReady()
      {
         isProviderReady = true;

         if (Util.Config.DEBUG)
            Debug.Log("Provider ready - all stations loaded.", this);

         //Player.Station = Set.StationByIndex(PlayRandom, -1, getFilter());
         player.Station = set.NextStation(playRandom, getFilter());

         if (!Util.Helper.isEditorMode && playOnStart && !playedOnStart)
         {
            playedOnStart = true;
            Play();
         }

         OnProviderReadyEvent?.Invoke();

         OnProviderReady?.Invoke();
      }

      private void onStationsChange()
      {
         if (Util.Config.DEBUG)
            Debug.Log("onStationsChange SP", this);

         //Player.Station = Set.StationByIndex(PlayRandom, -1, getFilter());
         player.Station = set.NextStation(playRandom, getFilter());

         if (!Util.Helper.isEditorMode && playOnStart && !playedOnStart)
         {
            playedOnStart = true;
            Play();
         }

         OnStationsChanged?.Invoke();

         OnStationsChange?.Invoke();
      }

      private void onFilterChange()
      {
         if (isProviderReady)
            player.Station = set.StationFromIndex(playRandom, -1, getFilter());
         //Player.Station = Set.NextStation(PlayRandom, getFilter());

         OnFilterChanged.Invoke();

         OnFilterChange?.Invoke();
      }

      private void onStationChange(Model.RadioStation newStation)
      {
         OnStationChanged?.Invoke(newStation.Name, newStation.GetHashCode());

         OnStationChange?.Invoke(newStation);
      }

      #endregion


      #region Set wrapper

      public System.Collections.Generic.List<Model.RadioStation> Stations => set != null ? set.Stations : new System.Collections.Generic.List<Model.RadioStation>();

      public System.Collections.Generic.List<Crosstales.Radio.Model.RadioStation> RandomStations => set != null ? set.RandomStations : new System.Collections.Generic.List<Model.RadioStation>();

      public bool isReady => set != null && set.isReady;

      public int CurrentStationIndex
      {
         get => set != null ? set.CurrentStationIndex : 0;
         set
         {
            if (set != null)
               set.CurrentStationIndex = value;
         }
      }

      public int CurrentRandomStationIndex
      {
         get => set != null ? set.CurrentRandomStationIndex : 0;
         set
         {
            if (set != null)
               set.CurrentRandomStationIndex = value;
         }
      }

      public System.Collections.Generic.List<Model.RadioStation> GetStations(bool random = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.GetStations(random, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public int CountStations(Model.RadioFilter filter = null)
      {
         return set != null ? set.CountStations(getFilter(filter)) : 0;
      }

      public Model.RadioStation StationFromIndex(bool random = false, int index = -1, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationFromIndex(random, index, getFilter(filter)) : null;
      }

      public Crosstales.Radio.Model.RadioStation StationFromHashCode(int hashCode)
      {
         return set != null ? set.StationFromHashCode(hashCode) : null;
      }

      public Model.RadioStation NextStation(bool random = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.NextStation(random, getFilter(filter)) : null;
      }

      public Model.RadioStation PreviousStation(bool random = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.PreviousStation(random, getFilter(filter)) : null;
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByName(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByName(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByURL(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByURL(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByFormat(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByFormat(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByStation(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByStation(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByBitrate(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByBitrate(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByGenres(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByGenres(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByCities(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByCities(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Crosstales.Radio.Model.RadioStation> StationsByCountries(bool desc = false, Crosstales.Radio.Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByCountries(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByLanguages(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByLanguages(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public System.Collections.Generic.List<Model.RadioStation> StationsByRating(bool desc = false, Model.RadioFilter filter = null)
      {
         return set != null ? set.StationsByRating(desc, getFilter(filter)) : new System.Collections.Generic.List<Model.RadioStation>();
      }

      public void Load()
      {
         if (set != null)
            set.Load();
      }

      public void Save(string path, Model.RadioFilter filter = null)
      {
         if (set != null)
            set.Save(path, filter);
      }

      public void RandomizeStations(bool resetIndex = true)
      {
         if (set != null)
            set.RandomizeStations();
      }

      #endregion
   }
}
// © 2016-2020 crosstales LLC (https://www.crosstales.com)
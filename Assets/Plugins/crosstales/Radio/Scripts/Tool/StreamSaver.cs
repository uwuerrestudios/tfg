﻿using UnityEngine;

namespace Crosstales.Radio.Tool
{
   /// <summary>
   /// Saves the streams of a player as audio files in the WAV-format.
   /// NOTE: Copyright laws for music are VERY STRICT and MUST BE respected! If you save music, make sure YOU have the RIGHT to do so! crosstales LLC denies any responsibility for YOUR actions with this tool - use it at your OWN RISK!
   /// For more, see  https://en.wikipedia.org/wiki/Radio_music_ripping and the rights applying to your country.
   /// </summary>
   [RequireComponent(typeof(AudioSource))]
   [HelpURL("https://crosstales.com/media/data/assets/radio/api/class_crosstales_1_1_radio_1_1_tool_1_1_stream_saver.html")]
   public class StreamSaver : MonoBehaviour
   {
      #region Variables

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("Player")] [Tooltip("Origin Player."), SerializeField]
      private BasePlayer player;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("SilenceSource")] [Tooltip("Silence the origin (default: true)."), SerializeField]
      private bool silenceSource = true;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("OutputPath")] [Tooltip("Output path for the audio files."), SerializeField]
      private string outputPath;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("RecordStartDelay")] [Tooltip("Record delay in seconds before start saving the audio (default: 0)."), SerializeField, Range(0f, 20f)]
      private float recordStartDelay = 0f;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("RecordStopDelay")] [Tooltip("Record delay in seconds before stop saving the audio (default: 0)."), SerializeField, Range(0f, 20f)]
      private float recordStopDelay = 0f;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("AddStationName")] [Tooltip("Add the station name to the audio files (default: true)."), SerializeField]
      private bool addStationName = true;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("AddTimestamp")] [Tooltip("Add the current timestamp to the audio files (default: false)."), SerializeField]
      private bool addTimestamp = false;

      private System.IO.FileStream fileStream;

      private const int HEADER_SIZE = 44; //default for uncompressed wav
      private const float RESCALE_FACTOR = 32767f;

      private AudioSource audioSource;

      private bool recOutput = false;
      private bool stopped = true;

      private long dataPosition = 0;

      private string fileName;

      #endregion


      #region Properties

      /// <summary>Origin Player.</summary>
      public BasePlayer Player
      {
         get => player;
         set => player = value;
      }

      /// <summary>Silence the origin.</summary>
      public bool SilenceSource
      {
         get => silenceSource;
         set => silenceSource = value;
      }

      /// <summary>Output path for the audio files.</summary>
      public string OutputPath
      {
         get => outputPath;
         set => outputPath = Util.Helper.ValidatePath(value);
      }

      /// <summary>Record delay in seconds before start saving the audio (range 0-20).</summary>
      public float RecordStartDelay
      {
         get => recordStartDelay;
         set => recordStartDelay = Mathf.Clamp(value, 0, 20);
      }

      /// <summary>Record delay in seconds before stop saving the audio (range 0-20).</summary>
      public float RecordStopDelay
      {
         get => recordStopDelay;
         set => recordStopDelay = Mathf.Clamp(value, 0, 20);
      }

      /// <summary>Add the station name to the audio files.</summary>
      public bool AddStationName
      {
         get => addStationName;
         set => addStationName = value;
      }

      /// <summary>Add the current timestamp to the audio files.</summary>
      public bool AddTimestamp
      {
         get => addTimestamp;
         set => addTimestamp = value;
      }

      #endregion


      #region MonoBehaviour methods

      public void Awake()
      {
         audioSource = GetComponent<AudioSource>();
         audioSource.playOnAwake = false;
         audioSource.Stop(); //always stop the AudioSource at startup
      }

      public void Start()
      {
         if (player == null)
         {
            Debug.LogWarning("No 'Player' added to the StreamSaver!", this);
         }
         else
         {
            player.CaptureDataStream = true;
            player.LegacyMode = false;
         }

         if (string.IsNullOrEmpty(outputPath))
            Debug.LogWarning("No 'OutputPath' added to the StreamSaver, saving in the project root!", this);
      }

      public void Update()
      {
         if (player != null && player.isAudioPlaying)
         {
            if (stopped)
            {
               stopped = false;

               if (player.DataStream != null)
                  dataPosition = player.DataStream.Position;

               AudioClip myClip = AudioClip.Create(player.Station.Name, int.MaxValue, player.Station.Channels, player.Station.SampleRate, true, readPCMData);

               audioSource.clip = myClip;

               audioSource.Play();

               if (silenceSource)
                  player.Silence();
            }
         }
         else
         {
            if (!stopped)
            {
               audioSource.Stop();
               audioSource.clip = null;
               stopped = true;
            }
         }
      }

      public void OnEnable()
      {
         if (player != null)
         {
            player.OnAudioEnd += onAudioEnd;
            player.OnNextRecordChange += onNextRecordChange;
         }
      }

      public void OnDisable()
      {
         if (player != null)
         {
            player.OnAudioEnd -= onAudioEnd;
            player.OnNextRecordChange -= onNextRecordChange;
         }

         closeFile();

         audioSource.Stop();
         audioSource.clip = null;
         stopped = true;
      }

      public void OnValidate()
      {
         if (!string.IsNullOrEmpty(outputPath))
            outputPath = Util.Helper.ValidatePath(outputPath);
      }

      #endregion


      #region Private methods

      private void openFile()
      {
         if (Util.Config.DEBUG)
            Debug.Log("openFile: " + fileName, this);

         if (fileStream?.CanWrite == true)
            closeFile();

         try
         {
            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(fileName));

            fileStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create);
            const byte emptyByte = 0;

            for (int ii = 0; ii < HEADER_SIZE; ii++) //preparing the header
            {
               fileStream.WriteByte(emptyByte);
            }

            recOutput = true;
         }
         catch (System.Exception ex)
         {
            Debug.LogError("Could not open file '" + fileName + "': " + ex, this);
         }
      }

      private void convertAndWrite(float[] dataSource)
      {
         if (fileStream?.CanWrite == true)
         {
            short[] intData = new short[dataSource.Length];
            byte[] bytesData = new byte[dataSource.Length * 2];

            for (int ii = 0; ii < dataSource.Length; ii++)
            {
               intData[ii] = (short)(dataSource[ii] * RESCALE_FACTOR);
               byte[] byteArr = System.BitConverter.GetBytes(intData[ii]);
               byteArr.CopyTo(bytesData, ii * 2);
            }

            try
            {
               fileStream.Write(bytesData, 0, bytesData.Length);
            }
            catch (System.Exception ex)
            {
               Debug.LogError("Could write to file '" + fileName + "': " + ex, this);
            }
         }
      }

      private void closeFile()
      {
         if (Util.Config.DEBUG)
            Debug.Log("closeFile", this);

         recOutput = false;

         if (fileStream?.CanWrite == true)
         {
            try
            {
               fileStream.Seek(0, System.IO.SeekOrigin.Begin);

               byte[] riff = System.Text.Encoding.UTF8.GetBytes("RIFF");
               fileStream.Write(riff, 0, 4);

               byte[] chunkSize = System.BitConverter.GetBytes(fileStream.Length - 8);
               fileStream.Write(chunkSize, 0, 4);

               byte[] wave = System.Text.Encoding.UTF8.GetBytes("WAVE");
               fileStream.Write(wave, 0, 4);

               byte[] fmt = System.Text.Encoding.UTF8.GetBytes("fmt ");
               fileStream.Write(fmt, 0, 4);

               byte[] subChunk1 = System.BitConverter.GetBytes(16);
               fileStream.Write(subChunk1, 0, 4);

               const ushort one = 1;

               byte[] audioFormat = System.BitConverter.GetBytes(one);
               fileStream.Write(audioFormat, 0, 2);

               byte[] numChannels = System.BitConverter.GetBytes(player.Station.Channels);
               fileStream.Write(numChannels, 0, 2);

               byte[] sampleRate = System.BitConverter.GetBytes(player.Station.SampleRate); //Hz?
               fileStream.Write(sampleRate, 0, 4);

               byte[] byteRate = System.BitConverter.GetBytes(player.Station.SampleRate * player.Station.Channels * 2);

               fileStream.Write(byteRate, 0, 4);

               ushort blockAlign = (ushort)(player.Station.Channels * 2);
               fileStream.Write(System.BitConverter.GetBytes(blockAlign), 0, 2);

               const ushort bps = 16;
               byte[] bitsPerSample = System.BitConverter.GetBytes(bps);
               fileStream.Write(bitsPerSample, 0, 2);

               byte[] datastring = System.Text.Encoding.UTF8.GetBytes("data");
               fileStream.Write(datastring, 0, 4);

               byte[] subChunk2 = System.BitConverter.GetBytes(fileStream.Length - HEADER_SIZE);
               fileStream.Write(subChunk2, 0, 4);
            }
            catch (System.Exception ex)
            {
               Debug.LogError("Could write header for file '" + fileName + "': " + ex, this);
            }
#if (!UNITY_WSA && !UNITY_WEBGL) || UNITY_EDITOR
            finally
            {
               try
               {
                  fileStream.Close();
               }
               catch (System.Exception ex)
               {
                  Debug.LogError("Could close file '" + fileName + "': " + ex, this);
               }
            }
#endif
         }
      }

      private void readPCMData(float[] data)
      {
         if (player.isAudioPlaying && player.DataStream != null)
         {
            byte[] buffer = new byte[data.Length * 2];

            int count;
            long tempPosition = player.DataStream.Position;
            player.DataStream.Position = dataPosition;

            if ((count = player.DataStream.Read(buffer, 0, buffer.Length)) > 0)
            {
               float[] floats = Util.Helper.ConvertByteArrayToFloatArray(buffer, count);
#if CT_BLOCKCOPY
               //System.Buffer.BlockCopy(new float[data.Length], 0, data, 0, data.Length); //TODO new, test it!
               System.Buffer.BlockCopy(floats, 0, data, 0, data.Length * 4);
#else
               System.Array.Copy(floats, 0, data, 0, data.Length);
#endif
               if (recOutput)
                  convertAndWrite(floats);

               dataPosition += count;
            }

            player.DataStream.Position = tempPosition;
         }
         else
         {
#if CT_BLOCKCOPY
            //System.Buffer.BlockCopy(new float[data.Length], 0, data, 0, data.Length); //TODO new, test it!
            System.Buffer.BlockCopy(new float[data.Length], 0, data, 0, data.Length * 4);
#else
            System.Array.Copy(new float[data.Length], 0, data, 0, data.Length);
#endif
         }
      }

      #endregion


      #region Callback methods

      private void onAudioEnd(Model.RadioStation station)
      {
         if (Util.Config.DEBUG)
            Debug.Log("onAudioEnd", this);

         closeFile();
      }

      private void onNextRecordChange(Model.RadioStation station, Model.RecordInfo nextRecord, float delay)
      {
         if (Util.Config.DEBUG)
            Debug.Log("onNextRecordChange: " + delay, this);

         if (delay > 0f)
            Invoke(nameof(closeFile), delay - recordStopDelay - 0.2f);

         string _name = string.IsNullOrEmpty(nextRecord.Artist) || string.IsNullOrEmpty(nextRecord.Title) ? station.Name + " - " + System.DateTime.Now.ToString("yyyyMMdd HH_mm_ss") + ".wav" : (addStationName ? station.Name + " - " : string.Empty) + (addTimestamp ? System.DateTime.Now.ToString("yyyyMMdd HH_mm_ss") + " - " : string.Empty) + nextRecord.Artist + " - " + nextRecord.Title + ".wav";
         fileName = Util.Helper.ValidateFile(outputPath + _name);
         Invoke(nameof(openFile), delay + recordStartDelay + 0.2f);
      }

      #endregion
   }
}
// © 2017-2020 crosstales LLC (https://www.crosstales.com)
﻿using UnityEngine;

namespace Crosstales.Radio.Tool
{
   /// <summary>Loudspeaker for a player.</summary>
   [RequireComponent(typeof(AudioSource))]
   [HelpURL("https://www.crosstales.com/media/data/assets/radio/api/class_crosstales_1_1_radio_1_1_tool_1_1_loudspeaker.html")]
   public class Loudspeaker : MonoBehaviour
   {
      #region Variables

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("Player")] [Tooltip("Origin Player."), SerializeField]
      private BasePlayer player;

      [UnityEngine.Serialization.FormerlySerializedAsAttribute("SilenceSource")] [Tooltip("Silence the origin (default: true)."), SerializeField]
      private bool silenceSource = true;

      private AudioSource audioSource;
      private bool stopped = true;
      private long dataPosition = 0;

      #endregion


      #region Properties

      /// <summary>Origin Player.</summary>
      public BasePlayer Player
      {
         get => player;
         set => player = value;
      }

      /// <summary>Silence the origin.</summary>
      public bool SilenceSource
      {
         get => silenceSource;
         set => silenceSource = value;
      }

      #endregion


      #region MonoBehaviour methods

      public void Awake()
      {
         audioSource = GetComponent<AudioSource>();
         audioSource.playOnAwake = false;
         audioSource.Stop(); //always stop the AudioSource at startup
      }

      public void Start()
      {
         if (player == null)
         {
            Debug.LogWarning("No 'Player' added to the Loudspeaker!", this);
         }
         else
         {
            player.CaptureDataStream = true;
         }
      }

      public void Update()
      {
         if (player != null && player.isAudioPlaying)
         {
            if (stopped)
            {
               stopped = false;

               if (player.DataStream != null)
                  dataPosition = player.DataStream.Position;

               AudioClip myClip = AudioClip.Create(player.Station.Name, int.MaxValue, player.Station.Channels, player.Station.SampleRate, true, readPCMData);

               audioSource.clip = myClip;

               audioSource.Play();

               if (silenceSource)
                  player.Silence();
            }
         }
         else
         {
            if (!stopped)
            {
               audioSource.Stop();
               audioSource.clip = null;
               stopped = true;
            }
         }
      }

      public void OnDisable()
      {
         audioSource.Stop();
         audioSource.clip = null;
         stopped = true;
      }

      #endregion


      #region Private methods

      private void readPCMData(float[] data)
      {
         if (player.isAudioPlaying && player.DataStream != null)
         {
            byte[] buffer = new byte[data.Length * 2];

            int count;
            long tempPosition = player.DataStream.Position;
            player.DataStream.Position = dataPosition;

            if ((count = player.DataStream.Read(buffer, 0, buffer.Length)) > 0)
            {
#if CT_BLOCKCOPY
               //System.Buffer.BlockCopy(new float[data.Length], 0, data, 0, data.Length); //TODO new, test it!
               System.Buffer.BlockCopy(Util.Helper.ConvertByteArrayToFloatArray(buffer, count), 0, data, 0, data.Length * 4);
#else
               System.Array.Copy(Util.Helper.ConvertByteArrayToFloatArray(buffer, count), 0, data, 0, data.Length);
#endif
               dataPosition += count;
            }

            player.DataStream.Position = tempPosition;
         }
         else
         {
#if CT_BLOCKCOPY
            //System.Buffer.BlockCopy(new float[data.Length], 0, data, 0, data.Length); //TODO new, test it!
            System.Buffer.BlockCopy(new float[data.Length], 0, data, 0, data.Length * 4);
#else
            System.Array.Copy(new float[data.Length], 0, data, 0, data.Length);
#endif
         }
      }

      #endregion
   }
}
// © 2017-2020 crosstales LLC (https://www.crosstales.com)
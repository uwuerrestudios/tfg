using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
  public static GameController instance = (GameController)null;
  public SoundPlayer m_soundPlayer;
  public Animator transicion, mecha;
  public float tiempoTransicion = 1;
  public GameObject opciones;
  bool estado = false;
  void Start() {
    Cursor.lockState = CursorLockMode.Locked;
    Scene currentScene = SceneManager.GetActiveScene();
    string sceneName = currentScene.name;
    if (sceneName != "Juego") {
      InvokeRepeating("activarMecha", 2.0f, 20f);  
    }
  }
  void Update() {
    
  }

  public void Jugar() {
    CargarJugar();
  }

  public void Salir() {
    Application.Quit();
  }

  public void Opciones() {
    opciones.SetActive(true);
  }

  public void Atras() {
    opciones.SetActive(false);
  }

  void CargarJugar() {
    StartCoroutine(CargarLvl(SceneManager.GetActiveScene().buildIndex + 1));
  }
  void activarMecha() {
    estado = !estado;
    mecha.SetBool("active", estado);
  }
  IEnumerator CargarLvl(int lvlIndex) {
    transicion.SetTrigger("start");
    yield return new WaitForSeconds(tiempoTransicion);
    SceneManager.LoadScene(lvlIndex);
  }
}

using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.ColliderEvent;
using HTC.UnityPlugin.Pointer3D;
using HTC.UnityPlugin.VRModuleManagement;
using HTC.UnityPlugin.Utility;
using cakeslice;

public class BordeObjetos : MonoBehaviour
	, IColliderEventPressEnterHandler
	, IColliderEventPressExitHandler
	, IPointer3DPressEnterHandler
  , IColliderEventHoverEnterHandler
  , IColliderEventHoverExitHandler
{
  public enum Anims {
    Protector,
    Pin,
    Tapa,
    Filtro,
    Nada
  }

	private Outline outline_interac;
	private List<string> interactuables;
  private bool estado = false;
  public Animator protector, pin, tapa, filtro;
  public GameObject textoColision;
  public Anims objeto;
  public Pointer3DRaycaster raycaster;
  public Transform reticleForDefaultRay;
  public Transform reticleForCurvedRay;
  public GameObject hitTarget;
  // Start is called before the first frame update
  void Start() {
		outline_interac = gameObject.GetComponent<Outline>();
    outline_interac.enabled = false;
		interactuables = new List<string>();
		interactuables.Add("Slot_Left_Panel");
		interactuables.Add("Torre");
		interactuables.Add("Slot_Bottom_Dust_Filter");
		interactuables.Add("Shield");
		interactuables.Add("Pin");
		interactuables.Add("MainBoard");
		interactuables.Add("Base_btn_luz_1");
		interactuables.Add("Base_btn_luz_2");
		interactuables.Add("Base_low");
		interactuables.Add("Base02_low");
    textoColision.gameObject.SetActive(false);
  }
	private void LateUpdate() {
    HoverPointer();
	}
	public void OnPointer3DPressEnter(Pointer3DEventData eventData) {
    PlayAnim(objeto);
    estado = !estado;
    Debug.Log(eventData.pointerEnter);
	}
  public void OnColliderEventPressEnter(ColliderButtonEventData eventData)  {
    PlayAnim(objeto);
  }
  public void OnColliderEventPressExit(ColliderButtonEventData eventData) {
    outline_interac.enabled = false;
	}

  public void OnColliderEventHoverEnter(ColliderHoverEventData eventData) {
    Debug.Log("collision");
    textoColision.gameObject.SetActive(true);
  }
  public void OnColliderEventHoverExit(ColliderHoverEventData eventData) {
    Debug.Log("Fin collision");
    textoColision.gameObject.SetActive(false);
  }

	void PlayAnim(Anims objeto) {
		switch (objeto) {
			case Anims.Tapa:
        tapa.SetTrigger("activo");
        tapa.SetBool("estado", estado);
        Debug.Log(estado);
				break;		
			case Anims.Protector:
				pin.SetTrigger("active");
        StartCoroutine(Wait2secs());
				break;	
      case Anims.Filtro:
        filtro.SetTrigger("activo");
        filtro.SetBool("estado", estado);
        break;  	
		}
	}
 
	IEnumerator Wait2secs() {
		yield return new WaitForSeconds(3f);
		protector.SetTrigger("active");
	}

	protected void HoverPointer() {
    var points = raycaster.BreakPoints;
    var pointCount = points.Count;
    var result = raycaster.FirstRaycastResult();

    var isCurvedRay = raycaster.CurrentSegmentGenerator() != null;

    if (reticleForDefaultRay != null) { reticleForDefaultRay.gameObject.SetActive(!isCurvedRay); }
    if (reticleForCurvedRay != null) { reticleForCurvedRay.gameObject.SetActive(isCurvedRay); }

    var targetReticle = isCurvedRay ? reticleForCurvedRay : reticleForDefaultRay;
    if (result.isValid) { 
      if (targetReticle != null) {
        targetReticle.position = result.worldPosition;
        targetReticle.rotation = Quaternion.LookRotation(result.worldNormal, raycaster.transform.forward);
      }

      hitTarget = result.gameObject;
			if (interactuables.Contains(hitTarget.gameObject.name)) {
				hitTarget.GetComponent<Outline>().enabled = true;
				Debug.Log(hitTarget);
      }
      outline_interac.enabled = false;
    }
	}
}

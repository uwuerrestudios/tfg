using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using HTC.UnityPlugin.Pointer3D;
using HTC.UnityPlugin.ColliderEvent;
using UnityEngine;

public class SoundPlayer : MonoBehaviour, IColliderEventPressEnterHandler, IColliderEventPressExitHandler,IPointer3DPressEnterHandler {
  public enum Objeto {
    Altavoz,
    PC,
    Luz,
    Entorno,
    QuitarTapa,
    Nada

  }

  private bool isEncendido;
  public Objeto objeto;
  FMOD.Studio.EventInstance Ventilador, Encendido, Fondo, Luz, QuitarTapa;
  private void Awake() {
    
  }
  private void Start() {
		Ventilador = FMODUnity.RuntimeManager.CreateInstance("event:/Vent");
		Encendido = FMODUnity.RuntimeManager.CreateInstance("event:/Encendido");
		Fondo = FMODUnity.RuntimeManager.CreateInstance("event:/Fondo");
		Luz = FMODUnity.RuntimeManager.CreateInstance("event:/Luz");
    QuitarTapa = FMODUnity.RuntimeManager.CreateInstance("event:/QuitarTapa");
    isEncendido = false;
	}

  void Update() {
    FMODUnity.RuntimeManager.AttachInstanceToGameObject(Ventilador, GetComponent<Transform>(), GetComponent<Rigidbody>());
    FMODUnity.RuntimeManager.AttachInstanceToGameObject(Encendido, GetComponent<Transform>(), GetComponent<Rigidbody>());
    FMODUnity.RuntimeManager.AttachInstanceToGameObject(Fondo, GetComponent<Transform>(), GetComponent<Rigidbody>());
    FMODUnity.RuntimeManager.AttachInstanceToGameObject(Luz, GetComponent<Transform>(), GetComponent<Rigidbody>());
    FMODUnity.RuntimeManager.AttachInstanceToGameObject(QuitarTapa, GetComponent<Transform>(), GetComponent<Rigidbody>());
  }

  public void OnPointer3DPressEnter(Pointer3DEventData eventData) {
    PlaySound(objeto);
  }

  public void OnColliderEventPressEnter(ColliderButtonEventData eventData) {
    PlaySound(objeto);
  }
  public void OnColliderEventPressExit(ColliderButtonEventData eventData) { }

  public void PlaySound(Objeto obj) {
    switch (obj) {
      case Objeto.PC:
        Debug.Log(isEncendido);
        isEncendido = !isEncendido;
        if (isEncendido) {
          Encendido.start();
        }
        if (FmodExtensions.IsPlaying(Ventilador)) {
          Ventilador.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        } else {
          Ventilador.start();
        }
        break;
      case Objeto.Altavoz:
        if (FmodExtensions.IsPlaying(Fondo)) {
          Fondo.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        } else {
          Fondo.start();
        }
        break;
      case Objeto.Luz:
        Luz.start();
        break;  
      case Objeto.QuitarTapa:
        QuitarTapa.start();
        break;  
    }
  }

}


public class FmodExtensions {
  public static bool IsPlaying(FMOD.Studio.EventInstance instance) {
    FMOD.Studio.PLAYBACK_STATE state;
    instance.getPlaybackState(out state);
    return state != FMOD.Studio.PLAYBACK_STATE.STOPPED;
  }
}
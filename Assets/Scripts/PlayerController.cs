using System;
using System.Collections;
using System.Collections.Generic;
using HTC.UnityPlugin.ColliderEvent;
using UnityEngine;

public class PlayerController : MonoBehaviour{

  //These variables will all be set in the Inspector tab of Unity's Editor by either us, or the 'FMODStudioFootstepsEditor' script.
  [Header("Ajustes FMOD")]
  [SerializeField] [FMODUnity.EventRef] private string FootstepsEventPath;
  [Header("Playback Settings")]
  [SerializeField] private float StepDistance;
  private bool isMoving = false;
	public Transform body;
	
  void Start() {
		InvokeRepeating("PlayFootstep", 0, StepDistance);
  }

    // Update is called once per frame
	void Update() {
		if (Input.GetKey(KeyCode.W)) {
			isMoving = true;
		}	else {
			isMoving = false;
		}	
	}

  void PlayFootstep() {
		if (isMoving) {
			FMODUnity.RuntimeManager.PlayOneShot(FootstepsEventPath);
		}
  }

	void OnDisable() {
		isMoving = false;	
	}



}
